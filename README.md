# RIV TA verifieringsscript

Detta är Python-script för följande ändamål:

* att verifiera att en tjänstedomäns uppfyller RIV Tekniska Anvisningar 
* att skapa releasepaket.

Scripten behöver installeras med pip innan de kan köras. Se instruktioner på http://bitbucket.org/rivta-tools/verify-scripts/wiki

Instruktioner för utvecklare av scripten finns i filen README-DEV.