# Instruktioner för utvecklare

Koden paketeras med Python Setuptools, och scripten kan inte köras direkt från källkodsmappen. För att kunna köra scripten  
behöver paketet först installeras, och scripten köras från den mapp de installerats till 
(Pythons installationsmapp\Scripts på Windows eller /usr/local/bin på Mac). 
För att slippa installera om koden efter varje kodändring, kan man i utvecklingsmiljöer  
 göra en "developer installation". Det skapar "skuggfiler" i installationskatalogen, som pekar direkt på filerna i din källkodsmapp.
 På så sätt kan du arbeta med koden och direkt exekvera den utan att behöva installera om.

Gör så här:

1. Klona detta git repository till en lokal arbetsmapp
2. Öppna en kommandoprompt, gå till arbetsmappen och skriv följande kommando:

```
python setup.py develop
```

## Stöd för Python 2 och 3
Scripten fungerar idag under både Python 2 och 3, med undantag för de versioner som inte stöds av lxml-biblioteket.
Då en sårbarhet har upptäckts i Python rekommenderas att använda version 3.11 eller senare. Vid uppdatering till 3.11 gick det inte längre att använda den utpekade versionen (3.4.4) av lxml. Problemet med lxml är nu löst (testat i Windows) genom att inte längre peka ut en specifik version av lxml i setup.py.  

Följande har vidtagits för att stödja både Python 2 och 3:

* print("") används alltid istället för print ""
* io.open används istället för open för att öppna filer. Detta gör att encoding kan anges som en parameter, vilket hjälper Python 2.x att läsa filer i UTF-8.
* HTTP-funktioner importeras med try/except, pga att funktioner flyttats runt mellan Pythonversioner
* Många filer använder "from __future__ import unicode_literals" för att stödja strängar med ÅÄÖ i Python 2.x. (u-prefixet ger syntax error i Python 3.1 och 3.2)


## Paketera
Gör så här för att paketera en ny release:

* uppdatera versionsnumret i setup.py (använd riktlinjerna från http://semver.org)
* committa dina ändringar med Git
* Använd följande kommando för att skapa paketet:

python setup.py sdist --formats=zip

* Ladda upp zip-filen till http://rivta.se/tools


## Open source
Hela projektet är open source under Apache License. Tänk på följande:

* Varje ny källkodsfil ska ha en licensheader enligt Apache instruktioner.
* Källkodsbibliotek som används ska nämnas med namn och licens i filen NOTICE.txt