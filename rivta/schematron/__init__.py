# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import os
from lxml import etree, isoschematron


class SchematronValidator:
    """Validerar ett xml-dokument mot en uppsättning Schematron-regler"""
    def __init__(self, xsl_path, rule_document_name):
        # open the Schematron rules file
        doc = etree.parse(xsl_path)
        self.schematron = isoschematron.Schematron(doc)
        self.rule_document = rule_document_name

    def validate(self, instance_path, args, domain):
        # open the xml document to validate
        full_path = os.path.join(domain.rootPath, instance_path)
        doc = etree.parse(full_path)

        # documented api is schematron.validate(doc), but it does not allow xslt parameters (domainName etc)
        # we need to call the internal method instead...
        validation_report = self.schematron._validator(doc, **args)

        self._create_issues(instance_path, validation_report, domain)

    def _create_issues(self, instance_path, report, domain):
        current_rule = ""

        for node in report.getroot():
            if node.tag == "{http://purl.oclc.org/dsdl/svrl}active-pattern":
                current_rule = node.attrib["name"]
            elif node.tag == "{http://purl.oclc.org/dsdl/svrl}successful-report":
                domain.append_warning(
                    message=node.find("{http://purl.oclc.org/dsdl/svrl}text").text.strip(),
                    rule_document=self.rule_document,
                    rulename=current_rule,
                    examined_path=instance_path
                )
            elif node.tag == "{http://purl.oclc.org/dsdl/svrl}failed-assert":
                domain.append_error(
                    message=node.find("{http://purl.oclc.org/dsdl/svrl}text").text.strip(),
                    rule_document=self.rule_document,
                    rulename=current_rule,
                    examined_path=instance_path
                )
