# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import unicode_literals
import os

from rivta import files

ruleDocument = "RIV TA Konfigurationsstyrning"


def checkdomain(domain):
    """Kontrollerar om en tjänstedomän uppfyller reglerna i Konfigurationsstyrning (ARK_0007)"""
    domain_parts = domain.name.split(':')
    domain_name_with_underscores = '_'.join(domain_parts)

    _check_docfile(domain, "TKB_" + domain_name_with_underscores + ".doc(x)?", True)
    _check_docfile(domain, "AB_" + domain_name_with_underscores + ".doc(x)?", True)

    _check_testsuite(domain)


def _check_docfile(domain, document_name, required):
    documents_directory = "docs"

    if not files.file_exists(domain.rootPath, documents_directory, document_name):
        domain.append_issue(
            message="Document missing.",
            examined_path=os.path.join(documents_directory, document_name),
            rule_document=ruleDocument,
            rulename="",
            is_error=required
        )


def _check_buildfile(domain, subfolder, filename):
    if not files.file_exists(domain.rootPath, subfolder, filename):
        domain.append_error(
            message="Build script is missing.",
            examined_path=os.path.join(subfolder, filename),
            rule_document=ruleDocument,
            rulename="",
        )


def _check_testsuite(domain):
    if not files.file_exists(domain.rootPath, "", "test-suite"):
        domain.append_warning(
            message="No test suite found.",
            examined_path="test-suite",
            rule_document=ruleDocument,
            rulename=""
        )
