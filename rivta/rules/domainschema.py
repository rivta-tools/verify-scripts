# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import unicode_literals
import os
import re

from rivta import files, xmlschema, schematron
from pkg_resources import resource_stream

schema_path = os.path.join("modules", "xmlschema", "schemas-all.xsd")
core_components_path = os.path.join("schemas", "core_components")

rule_document = "RIV TA Domänschema"


def checkdomain(domain):
    """Kontrollerar om en tjänstedomän uppfyller reglerna i RIV TA Domänschema (ARK_0006)"""
    file_pattern = domain.name.replace(":", "_") + "_\d\.\d\.xsd"
    matches = files.find_regex_matches(domain.rootPath, core_components_path, file_pattern)

    if len(matches) == 0:
        domain.append_warning(
            message="Missing domain schema.",
            examined_path=os.path.join(core_components_path, file_pattern),
            rule_document=rule_document,
            rulename="Regel #2"
        )
    else:
        relative_schema_path = os.path.join(core_components_path, matches[0])
        _validate_xmlschema(domain, relative_schema_path)
        _validate_rivtaschema(domain, relative_schema_path)


def _validate_xmlschema(domain, schema_to_validate):
    schema = resource_stream('rivta.xmlschema', 'schemas-all.xsd')
    validator = xmlschema.SchemaValidator(schema, "XML Schema 1.0")
    validator.validate(schema_to_validate, domain)


def _validate_rivtaschema(domain, schema_path):
    schematron_filepath = files.get_local_path("http://rivta.se/tools/python/data/rivtadomain21.sch")
    path, filename = os.path.split(schema_path)

    args = {
        "domainName": "'" + domain.name + "'"
    }

    version_match = re.search("\d\.\d", filename)
    args["majorVersion"] = "'" + version_match.group(0)[0:1] + "'"
    args["minorVersion"] = "'" + version_match.group(0)[2:3] + "'"

    validator = schematron.SchematronValidator(schematron_filepath, rule_document)
    validator.validate(schema_path, args, domain)


