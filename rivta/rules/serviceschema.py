# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from __future__ import unicode_literals
import os
import re

from rivta import files, xmlschema, schematron
from pkg_resources import resource_stream


rule_document = "RIV TA Tjänsteschema"


def checkdomain(domain):
    """Kontrollerar om en tjänstedomän uppfyller reglerna i RIV TA Tjänsteschmea (ARK_0005)"""
    interactions_path = os.path.join("schemas", "interactions")

    interaction_folders = files.find_regex_matches(domain.rootPath, interactions_path, ".*Interaction")

    for folder in interaction_folders:
        folder_path = os.path.join(interactions_path, folder)
        file_pattern = folder.replace("Interaction", "") + "(Responder|Initiator)_\d\.\d\.xsd"

        schema_files = files.find_regex_matches(domain.rootPath, folder_path, file_pattern)
        if len(schema_files) == 0:
            domain.append_warning(
                rule_document="RIV TA Tjänsteschema",
                rulename="Regel #2",
                message="Missing service schema: " + file_pattern,
                examined_path=os.path.join(folder_path, file_pattern)
            )
        else:
            for schema_file in schema_files:
                relative_file_path = os.path.join(folder_path, schema_file)
                _validate_xmlschema(domain, relative_file_path)
                _validate_rivtaschema(domain, relative_file_path)


def _validate_xmlschema(domain, schema_to_validate):
    schema = resource_stream('rivta.xmlschema', 'schemas-all.xsd')
    validator = xmlschema.SchemaValidator(schema, "XML Schema 1.0")
    validator.validate(schema_to_validate, domain)


def _validate_rivtaschema(domain, schema_path):
    schematron_filepath = files.get_local_path("http://rivta.se/tools/python/data/rivtaservice21.sch")

    args = {
        "domainName": "'" + domain.name + "'"
    }

    path, filename = os.path.split(schema_path)

    role_match = re.search("(Responder|Initiator)", filename)
    args["interactionRole"] = "'" + role_match.group(0) + "'"
    args["interactionName"] = "'" + filename[0: role_match.start(0)] + "'"
    args["operationName"] = "'" + filename[0: role_match.start(0)] + "'"

    version_match = re.search("\d\.\d", filename)
    args["majorVersion"] = "'" + version_match.group(0)[0:1] + "'"
    args["minorVersion"] = "'" + version_match.group(0)[2:3] + "'"

    validator = schematron.SchematronValidator(schematron_filepath, rule_document)
    validator.validate(schema_path, args, domain)
