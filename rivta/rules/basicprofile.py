# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import unicode_literals
import os
import re

from rivta import files, xmlschema, schematron
from pkg_resources import resource_stream

rule_document = "RIV TA Basic Profile"


def checkdomain(domain):
    """Kontrollerar om en tjänstedomän uppfyller reglerna i RIV TA Basic Profile 2.0 eller 2.1 (ARK_0002)"""
    interactions_path = os.path.join("schemas", "interactions")

    folders = files.find_regex_matches(domain.rootPath, interactions_path, ".*Interaction")

    for folder in folders:
        folder_path = os.path.join(interactions_path, folder)
        file_pattern = "(?i)" + folder + "_\d\.\d\_RIVTABP2\d.wsdl"

        wsdl_files = files.find_regex_matches(domain.rootPath, folder_path, file_pattern)
        if len(wsdl_files) == 0:
            domain.append_warning(
                rule_document="RIV TA Basic Profile",
                rulename="Regel #2",
                message="Missing wsdl file: " + file_pattern,
                examined_path=os.path.join(folder_path, file_pattern)
            )
        else:
            for wsdl_file in wsdl_files:
                relative_path = os.path.join(folder_path, wsdl_file)
                _validate_xmlschema(domain, relative_path)
                _validate_basicprofile(domain, relative_path)


def _validate_xmlschema(domain, wsdl_to_validate):
    schema = resource_stream('rivta.xmlschema', 'schemas-all.xsd')
    validator = xmlschema.SchemaValidator(schema, "WSDL 1.0")
    validator.validate(wsdl_to_validate, domain)


def _validate_basicprofile(domain, wsdlpath):
    if "rivtabp21" in wsdlpath.lower():
        schematron_path = files.get_local_path("http://rivta.se/tools/python/data/rivtabp21.sch")
    elif "rivtabp20" in wsdlpath.lower():
        schematron_path = files.get_local_path("http://rivta.se/tools/python/data/rivtabp20.sch")
        domain.append_warning(
            rule_document=rule_document,
            rulename="",
            message="Interaction uses legacy profile version",
            examined_path=wsdlpath
        )
    else:
        domain.append_warning(
            rule_document=rule_document,
            rulename="",
            message="Interaction uses unknown profile version",
            examined_path=wsdlpath
        )
        return

    args = {
        "domainName": "'" + domain.name + "'"
    }

    path, filename = os.path.split(wsdlpath)

    pos = filename.find("Interaction")
    args["responderOperationName"] = "'" + filename[0: pos] + "'"
    args["interactionName"] = "'" + filename[0: pos] + "'"

    version_match = re.search("\d\.\d", filename)
    args["majorVersion"] = "'" + version_match.group(0)[0:1] + "'"

    validator = schematron.SchematronValidator(schematron_path, rule_document)
    validator.validate(wsdlpath, args, domain)

