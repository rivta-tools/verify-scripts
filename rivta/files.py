# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import re
import tempfile

try:
    # Python 3 modules
    from urllib.request import urlopen
    from urllib.parse import urlparse
except ImportError:
    # Python 2 modules
    from urllib2 import urlopen
    from urlparse import urlparse


def file_exists(root_path, subfolder, search_pattern):
    """Returnerar True eller False, beroende på om någon fil matchar angiven regex"""
    result = find_regex_matches(root_path, subfolder, search_pattern)
    return (len(result) > 0)


def find_regex_matches(root_path, subfolder, search_pattern):
    """Returnerar filer som matchar en regular expression, t.ex. ".*\.bat$" """
    result = []

    full_path = os.path.join(root_path, subfolder)
    if not os.path.exists(full_path):
        return result

    dir_contents = os.listdir(full_path)

    for file_or_folder in dir_contents:
        if re.match(search_pattern, file_or_folder):
            result.append(file_or_folder)

    return result


def find_close_match(root_path, subfolder, filename):
    full_path = os.path.join(root_path, subfolder)
    if not os.path.exists(full_path):
        return

    existing_files = os.listdir(full_path)

    current_match = 0
    close_match = ""

    for file in existing_files:
        prefix = os.path.commonprefix([filename, file])
        if len(prefix) > current_match:
            current_match = len(prefix)
            close_match = file

    return close_match


def get_local_path(remote_url):
    urlparts = urlparse(remote_url)
    filename = urlparts.path.split('/')[-1]  # [-1] is last item of array
    local_path = os.path.join(tempfile.gettempdir(), filename)
    if not os.path.exists(local_path):
        print("Downloading " + remote_url)
        response=urlopen(remote_url)
        with open(local_path, "wb") as local_file:
            local_file.write(response.read())
    return local_path
