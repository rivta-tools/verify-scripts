# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re
import argparse


def domain_argument_type(input):
    """Används vid argument till script, där värdet ska vara ett domännamn"""
    if not re.match("^[a-z\.]+:[a-z]+(:[a-z]+)?$", input):
        raise argparse.ArgumentTypeError("Incorrect domain name, expected a:b[:c]")
    return input


class Domain:
    """Beskriver den domän som ska verifieras."""
    def __init__(self, name):
        self.name = name
        self.rootPath = ""
        self.issues = []

    def count_errors(self):
        errorcount = 0

        for issue in self.issues:
            if issue.error:
                errorcount += 1

        return errorcount

    def count_warnings(self):
        warningcount = 0

        for issue in self.issues:
            if not issue.error:
                warningcount += 1

        return warningcount

    def append_error(self, message, examined_path, rulename, rule_document):
        self.append_issue(message, examined_path, rulename, rule_document, True)

    def append_warning(self, message, examined_path, rulename, rule_document):
        self.append_issue(message, examined_path, rulename, rule_document, False)

    def append_issue(self, message, examined_path, rulename, rule_document, is_error):
        issue = Issue()
        issue.message = message
        issue.ruleName = rule_document + ": " + rulename
        issue.examinedPath = examined_path
        issue.error = is_error

        self.issues.append(issue)


class Issue:
    """Beskriver ett fel som upptäckts."""
    def __init__(self):
        self.message = ""
        self.error = False
        self.examinedPath = ""
        self.closeMatch = ""
        self.ruleName = ""
