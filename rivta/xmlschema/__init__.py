# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
from io import open
from lxml import etree


class SchemaValidator:
    """Validerar ett xml-dokument mot ett schema"""
    def __init__(self, xsd_file, rule_document):
        schema_doc = etree.parse(xsd_file)
        self.validator = etree.XMLSchema(schema_doc)
        self.rule_document = rule_document

    def validate(self, instance_path, domain):
        full_path = os.path.join(domain.rootPath, instance_path)
        with open(full_path, encoding="utf-8") as f:
            doc = etree.parse(f)

        try:
            self.validator.assertValid(doc)
        except etree.DocumentInvalid as e:
            domain.append_error(
                rule_document=self.rule_document,
                rulename="",
                message="Schema error: " + str(e),
                examined_path=instance_path
            )
