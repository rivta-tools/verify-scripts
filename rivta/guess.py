# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import re

from rivta.files import find_regex_matches

namePattern = re.compile('[a-zA-Z]{2,}')
versionPattern = re.compile('\d\.\d')


def guess_domain_name_and_print_results(domain):
    """Hanterar resultatet av identifieringen av domännamn."""
    candidates = _guess_domain_name(domain.rootPath)

    if len(candidates) == 1:
        domain.name = candidates[0]
        print("Assuming domain name: " + domain.name + " (override with -d).")
    else:
        print("No domain name supplied, and no name could be assumed by looking at file names.")
        print("Supply the domain name with the -d parameter.")
        if len(candidates) > 1:
            print("Suggestions: ")
            for candidate in candidates:
                print("* " + candidate)


def _guess_domain_name(root_path):
    """Försöker identifiera domänens namn, baserat på vissa utvalda filnamn."""
    core_components_path = os.path.join("schemas", "core_components")
    schema_files = find_regex_matches(root_path, core_components_path, ".*\.xsd$")
    tokenized_schema_files = _tokenize_and_strip_noise(schema_files, ["xsd", "enum"])

    documents_path = "docs"
    document_files = find_regex_matches(root_path, documents_path, "^(TKB|AB|VIS).*\.doc(x)?$")
    tokenized_document_files = _tokenize_and_strip_noise(document_files, ["TKB", "AB", "VIS", "doc", "docx"])

    tokenized_files = tokenized_schema_files + tokenized_document_files

    # tokenized_files innehåller nu alla xsd-filer i core_components, samt filer under docs
    # som börjar på TKB, AB eller VIS

    candidates = {"no:name:found": 0}

    for file in tokenized_files:
        # Join the file name parts with ':',
        # the result is a domain name candidate
        name = ":".join(file)

        # Increase points if we've seen this candidate before
        # otherwise add it with 1 point
        if name in candidates:
            candidates[name] += 1
        else:
            candidates[name] = 1

    max_points = max(candidates.values())

    # There could be two or more candidates with same points.
    # In this method we have no opinion about that
    winners = []

    for name in candidates:
        if candidates[name] == max_points:
            winners.append(name)

    return winners


def _tokenize_and_strip_noise(file_names, noise_words):
    """
    Delar upp filnamn i delar. "TKB_aaa_bbb_ccc.docx" blir t.ex. en array med [TKB, aaa, bbb, ccc, docx]
    Därefter används "noiseWords" för att filtrera bort ord som "TKB" och "docx"
    """
    parsed_files = []
    for file in file_names:
        all_parts = namePattern.findall(file)
        good_parts = []
        for part in all_parts:
            if part not in noise_words:
                good_parts.append(part)

        parsed_files.append(good_parts)

    return parsed_files

