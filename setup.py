#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Copyright 2023 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from setuptools import setup, find_packages

# NOTE: Unicode strings are not supported in this file
# See: https://docs.python.org/2/distutils/setupscript.html

setup(
    name="rivtascripts",
    version="2.1",
    author="Thomas Siltberg",
    author_email="thomas.siltberg@inera.se",

    packages=find_packages(),

    install_requires=['lxml'],
    package_data={
        'rivta.xmlschema': ['*.xsd'],
    },
    scripts=['scripts/createRivtaArchive.py', 'scripts/verifyRivtaDomain.py'],

    zip_safe=True,
    url="https://bitbucket.org/rivta-tools/domainScripts",

    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: Apache Software License',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.11',
    ],
)
