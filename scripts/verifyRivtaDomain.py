#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse

from operator import attrgetter

from rivta import Domain, domain_argument_type
from rivta.guess import guess_domain_name_and_print_results
from rivta.rules import run_tests


def print_issues(domain):
    for issue in sorted(domain.issues, key=attrgetter('ruleName', 'examinedPath')):
        print("")

        if issue.error:
            print("ERROR: " + issue.message)
        else:
            print("WARNING: " + issue.message)

        print("Rule: " + issue.ruleName)
        print("Examined path: " + issue.examinedPath)


parser = argparse.ArgumentParser(description="Verifies the domain against the RIV TA rules.")

parser.add_argument('-d', '--domain', type=domain_argument_type,
                    help='The name of the domain, in a:b:c format')

parser.add_argument('rootdir',
                    help='Base folder for the domain (trunk or a tag directory). Specify "." for current directory.')

args = parser.parse_args()

domain = Domain(args.domain)
domain.rootPath = args.rootdir

if domain.name is None:
    guess_domain_name_and_print_results(domain)

# Check again...
if domain.name is None:
    exit(-1)

run_tests(domain)
print_issues(domain)

print("")
print("There were {0} errors and {1} warnings.".format(domain.count_errors(), domain.count_warnings()))

exit(domain.count_errors())


