#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# Copyright 2015 Inera AB
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import argparse
import os
import zipfile

from rivta import Domain, domain_argument_type
from rivta.guess import guess_domain_name_and_print_results
from rivta.rules import run_tests

parser = argparse.ArgumentParser(description="Creates a zip archive of the domain.")

parser.add_argument('-d', '--domain', type=domain_argument_type,
                    help='The name of the domain, in a:b:c format')

parser.add_argument('rootdir',
                    help='Base folder for the domain (trunk or a tag directory). Specify "." for current directory.')

parser.add_argument('releaseName', help='Name/number of the release (1.0_RC3 etc.).')

parser.add_argument('-f', '--force', action='store_true', help='Create the archive even if there are validation errors.')

parser.add_argument('-z', '--zipDir', default='.', help='Directory to store zip.')


args = parser.parse_args()

domain = Domain(args.domain)
domain.rootPath = args.rootdir

if domain.name is None:
    guess_domain_name_and_print_results(domain)

# Check again...
if domain.name is None:
    exit(-1)

run_tests(domain)

errorCount = domain.count_errors()

if errorCount > 0 and not args.force:
    print("\nThere were " + str(errorCount) + " errors. No archive will be created (override with -f).")
    exit(errorCount)

zipName = 'ServiceContracts_{0}_{1}.zip'.format(domain.name.replace(':', '_'), args.releaseName)
zipPath = os.path.join(args.zipDir, zipName)

# Need to cd into rootPath to avoid complete path being recorded into the zip
user_dir = os.curdir
os.chdir(domain.rootPath)

archive = zipfile.ZipFile(zipPath, 'w', zipfile.ZIP_DEFLATED)

# Recurse and add files to zip
for root, dirs, files in os.walk(os.curdir):
    # Excluding files and folders beginning with a dot (.git, .DS_Store etc.)
    files = [f for f in files if not f[0] == '.']
    dirs[:] = [d for d in dirs if not d[0] == '.']
    for file in files:
        archive.write(os.path.join(root, file))

archive.close()

# cd back to where we started
os.chdir(user_dir)

print("Zip created: " + zipPath)


